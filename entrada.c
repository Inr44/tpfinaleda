#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#include <limits.h>

/**
Se representa el grafo como una lista de estructuras vertice, donde cada vertice
contiene una lista de aristas, el nombre de la ciudad, y la cantidad de vecinos
(el grado). Las aristas son estructuras que contienen el indice de la ciudad a 
la que van, y el costo.
*/
typedef struct  {
    int indice;
    int costo;
} Arista;

typedef struct {
    char etiqueta[30];
    int grado;
    Arista* vecinos;
} Vertice;


//// Entrada ////
/**
contar_ciudades: char[] -> int
Recibe una string con los nombres de todas las ciudades, y devuelve la cantidad de ciudades.
Para hacer esto cuenta los caracteres ',' que separan el nombre de cada ciudad.
*/
int contar_ciudades(char buffer[]) {
    int ciudades = 1;
    for (int i = 0; buffer[i]; ++i) {
        if (buffer[i] == ',') {
            ciudades++;
        }
    }
    return ciudades;
}

/**
parsear_ciudades: char[], Vertice*, int
Recibe una string con los nombres de todas las ciudades, la lista de ciudades, y la cantidad de ciudades.
Recorre la string copiando el nombre de cada ciudad en el nodo correspondiente, y a su vez reserva memoria
para los vecinos de cada ciudad (las aristas), e inicializa el grado en 0. Se reserva memoria para la
cantidad de ciudades menos 1, ya que esta es la maxima cantidad posible de vecinos (se considera
que el grafo no contiene bucles).
*/
void parsear_ciudades(char buffer[], Vertice* listaCiudades, int cantidadCiudades) {
    int c = 0, j = 0;
    listaCiudades[0].grado = 0;
    listaCiudades[0].vecinos = malloc(sizeof(Arista)*(cantidadCiudades-1));


    for(int i = 0; buffer[i]; i++){

        if (isspace(buffer[i])){
            listaCiudades[c].etiqueta[j] = '\0';
            c++;

            if (c < cantidadCiudades) {
                listaCiudades[c].grado = 0;
                listaCiudades[c].vecinos = malloc(sizeof(Arista)*(cantidadCiudades-1));
            }

            j = 0;
        }

        else if (buffer[i] != ',') {
            listaCiudades[c].etiqueta[j] = buffer[i];
            j++;
        }
    }
}

/**
buscar_ciudad: char*, vertice*, int -> int
Recibe una string con el nombre de una ciudad, la lista de ciudades, y la cantidad de ciudades.
Recorre el array que contiene todas las ciudades, revisando cual es el nodo cuya etiqueta coincide
con el nombre recibido, y devuelve el indice de este. En caso de no encontrarlo, devuelve un -1.
Comentario: Considere ordenar las ciudades alfabeticamente y hacer busqueda binaria para
optimizar la busqueda, pero luego de discutirlo con un miembro de la catedra termine decidiendo
implementar una busqueda lineal, ya que la diferencia en costo es pequeña en los casos chicos
para los que esta pensado el programa, y esta solucion es mas simple.
*/
int buscar_ciudad(char* ciudad1, Vertice* listaCiudades, int cantidadCiudades){
    for(int i = 0; i < cantidadCiudades; i++){
        if(strcmp(ciudad1, listaCiudades[i].etiqueta) == 0){
            return i;
        }
    }
    return -1;
}

/**
agregar_vecino: int, int, int, Vertice*
Recibe el indice de dos ciudades, el peso de la arista que las conecta, y el arreglo que
contiene a todas las ciudades. Completa los datos de la posicion correspondiente en el 
array de vecinos de cada ciudad, con el indice de la ciudad vecina y el costo de visitarla.
Luego aumenta en uno el grado del nodo.
*/
void agregar_vecino(int ciudad1, int ciudad2, int peso, Vertice* listaCiudades){
    int grado1, grado2;
    grado1 = listaCiudades[ciudad1].grado; // Estas dos asignaciones se utilizan para
    grado2 = listaCiudades[ciudad2].grado; // mejorar la legibilidad

    listaCiudades[ciudad1].vecinos[grado1].indice = ciudad2;
    listaCiudades[ciudad2].vecinos[grado2].indice = ciudad1;

    listaCiudades[ciudad1].vecinos[grado1].costo = peso;
    listaCiudades[ciudad2].vecinos[grado2].costo = peso;

    listaCiudades[ciudad1].grado++;
    listaCiudades[ciudad2].grado++;
}

/**
leer_aristas: FILE*, Vertice*, int
Recibe un puntero a un archivo abierto, la lista con las ciudades, y la cantidad de ciudades.
Se lee linea por linea del archivo con el formato esperado de "nombreciudad,nombreciudad2,costo",
hasta que la linea leida no respeta ese formato (cuando esto sucede, se presume que se alcanzo el
el final del archivo). Se buscan los indices de las ciudades leidas, y en caso de encontrarse,
se agregan las aristas correspondientes.
Comentario: Se supone que el formato del archivo de entrada es correcto, por lo tanto no se 
hace nada que corte la ejecucion en caso de encontrarse un error en la entrada. Los mensajes
de error fueron puestos para ayudar a debuggear, y no me parecio necesario removerlos.
*/
void leer_aristas(FILE* archivo, Vertice* listaCiudades, int cantidadCiudades){
    int peso, ciudad1, ciudad2;
    char NombreCiudad1[30], NombreCiudad2[30];
    while (fscanf(archivo,"%29[^,],%29[^,],%i\ns", NombreCiudad1, NombreCiudad2, &peso) == 3) {
        ciudad1 = buscar_ciudad(NombreCiudad1, listaCiudades, cantidadCiudades);
        ciudad2 = buscar_ciudad(NombreCiudad2, listaCiudades, cantidadCiudades);
        if (ciudad1 != -1 && ciudad2 != -1)
            agregar_vecino(ciudad1, ciudad2, peso, listaCiudades);
        else{
            printf("El formato del archivo es incorrecto \n");
            printf("%s, %s, %i \n", NombreCiudad1, NombreCiudad2, peso);
        }
    }
    if (!feof(archivo)) {
        printf("El formato del archivo es incorrecto \n");
        printf("EOF\n");
    }
}

/**
realocar_aristas: Vertice*, int
Se recibe el array con las ciudades y la cantidad de ciudades.
Se recorren las ciudades, reasignando la memoria de las aristas a un espacio
correspondiente a la cantidad de aristas, efectivamente liberando parte de la
memoria o no haciendo nada.
Comentario: Se comienza reservando memoria para el maximo posible de aristas, ya
que no tengo forma de anticipar cuantas aristas voy a tener hasta que las lea, 
pero luego, con el interes de no desperdiciar memoria y aprovechar la estructura
que estoy utilizando para representar el grafo, se libera la memoria se pidio de mas.
Si no se hiciera esto, el costo en memoria seria peor que el de una matriz de adyacencia,
perdiendo una de las ventajas que tiene mi representacion elegida.
Se considero usar una lista enlazada para la lectura y representar a los vecinos de esa
forma, o luego de leerlos iterar sobre la lista y pasarla a un array. Se termino eligiendo
esta implementacion por ser mas simple y practica (y tengo una preferencia respecto a 
usar arreglos, ya que tengo entendido que puede mejorar la eficiencia del programa).
*/
void realocar_aristas(Vertice* ciudades, int cantidadCiudades){
    for (int i = 0; i < cantidadCiudades; ++i) {
        ciudades[i].vecinos = realloc(ciudades[i].vecinos, (ciudades[i].grado) * sizeof(Arista));
    }
}

/**
comparar_aristas: const void*, const void* -> int
Recibe dos aristas como punteros void constantes (para respetar el prototipo de funcion
pedido por qsort), y devuelve 1, 0, o -1. La salida depende de si el costo de la primer arista
es menor, igual o mayor al de la segunda arista, respectivamente.
*/
int comparar_aristas(const void *arista1, const void *arista2) {
    Arista *aux1, *aux2;
    aux1 = (Arista*) arista1;
    aux2 = (Arista*) arista2;
    
    if (aux1->costo < aux2->costo)
        return -1;
    if (aux1->costo == aux2->costo)
        return 0;

    return 1;
}
/**
ordenar_aristas: Vertice*, int
Recibe el array con las ciudades, y la cantidad de ciudades. 
Recorre todas las ciudades, reordenando las aristas de menor a mayor costo.
Comentario: Se considero utilizar listas enlazadas para almacenar las aristas, y
realizar un insertion sort para ordenar las aristas. Termine eligiendo quicksort
porque no uso listas enlazadas, ya viene implementado en c, y me parecio la opcion
mas simple.
*/
void ordenar_aristas(Vertice* ciudades, int cantidadCiudades){
    for (int i = 0; i < cantidadCiudades; ++i) {
        qsort(ciudades[i].vecinos, ciudades[i].grado, sizeof(Arista), comparar_aristas);
    }
}

/**
entrada: char[], Vertice**
Recibe una string con el nombre del archivo de entrada, y un puntero a una lista de ciudades.
Abre y lee el archivo de entrada, ignorando la primer linea. Comienza contando la cantidad de
ciudades y reservando la memoria correspondiente. Luego procesa nuevamente la segunda linea
para parsear los nombres de las ciudades. Ignora la tercer linea, y comienza a parsear las aristas.
Finalmente, libera la memoria que reservo demas para las aristas, ordena las aristas, y cierra el archivo.
*/
int entrada (char nombreArchivo[], Vertice** listaCiudades){
    int cantidadCiudades;
    FILE* archivo;
    char buffer[400];

    archivo = fopen(nombreArchivo, "r");
    fscanf(archivo, "%*s\n");
    fgets(buffer, 400, archivo);

    cantidadCiudades = contar_ciudades(buffer);
    *listaCiudades = malloc(sizeof(Vertice)*(cantidadCiudades));
    parsear_ciudades(buffer, *listaCiudades, cantidadCiudades);
    fscanf(archivo, "%*s\n");
    
    leer_aristas(archivo, *listaCiudades, cantidadCiudades);

    realocar_aristas(*listaCiudades, cantidadCiudades);
    ordenar_aristas(*listaCiudades, cantidadCiudades);

    fclose(archivo);

    return cantidadCiudades;
}

///// Procesamiento y Salida ////

/**
mostrar_lista: Vertice*, int
Recibe el arreglo con las ciudades, y la cantidad de ciudades.
Muestra en pantalla los datos de cada vertice y las aristas que tiene.
Comentario: La funcion esta se utilizo para debuggear y verificar el funcionamiento
del programa, por eso no se llama en ningun momento. No me parecio mal dejarla 
comentada.
*/
/*
void mostrar_lista(Vertice* listaCiudades, int cantidadCiudades) {
    for(int i = 0; i < cantidadCiudades; i++){
        printf("Indice: %i \n Ciudad: %s \n  Grado: %i \n", i, listaCiudades[i].etiqueta, listaCiudades[i].grado);
        for(int j = 0; j < listaCiudades[i].grado; j++)
            printf("Arista: %i, Costo: %i, indice: %i \n", j, listaCiudades[i].vecinos[j].costo, listaCiudades[i].vecinos[j].indice);
        printf("\n\n");
    }
}
*/

/**
copiar_camino: Arista**, Arista**, int
Recibe dos arrays de punteros a aristas, que representan las aristas que forman
el ciclo, y copia el primero en el segundo. Tambien recibe la cantidad de ciudades,
que es igual al largo del camino.
*/
void copiar_camino(Arista* caminoActual[], Arista* mejorCamino[], int cantidadCiudades){
    for (int i = 0; i < cantidadCiudades; ++i) {
        mejorCamino[i] = caminoActual[i];
    }
}

/**
dfs: int, Vertice*, int, int*, Arista*[], Arista*[], int, int
Recibe una ciudad, la lista de ciudades, la cantidad de ciudades, el costo maximo
permitido(que comienza siendo INT_MAX, y luego cambia al costo del mejor camino
hasta el momento), el camino actual, que se representa como un arreglo de punteros
a las aristas que forman el camino,  el mejor camino, que se representa de la misma
forma, y es donde se guarda el mejor ciclo encontrado hsata el momento, el costo 
del camino que se esta revisando actualmente, y la cantidad de ciudades que se visitaron
hasta el momento.
Si ya fueron visitadas todas las ciudades, se revisan todas las aristas de la ciudad actual,
verificando que alguna de estas la conecte con la primer ciudad (que es desde donde
se empieza a buscar el ciclo), y que el costo de esta arista mas el costo actual no
supere el maximo. En caso de encontrar una arista que cumpla los requisitos, se
la agrega al camino actual, se copia el camino actual en mejor camino, y se
actualiza el costo maximo.
Si no fueron visitadas todas las ciudades, la funcion itera sobre las aristas 
conectadas con la ciudad actual, y en caso de que el costo actual no supere al 
costo maximo, y que la ciudad destino a la cual se llega mediante esa arista no 
haya sido visitada, agrega la arista al camino actual, y se llama recursivamente 
a si misma, reemplazando la ciudad actual por la ciudad siguiente, y sumandole 
el costo de la nueva arista al costo actual, asi como sumandole uno al contador 
de ciudades visitadas.
Una vez terminado el llamado recursivo, se marca la ciudad actual en el camino
actual como no visitada.
*/
void dfs(int ciudad, Vertice* ciudades, int cantidadCiudades, int *costomaximo,
         Arista* caminoActual[], Arista* mejorCamino[], int costoActual, int ciudadesVisitadas) {

    int ciudadSiguiente;
    if(ciudadesVisitadas == cantidadCiudades) {
        for(int i = 0; i < ciudades[ciudad].grado; i++) {
            if(ciudades[ciudad].vecinos[i].indice == 0  //tiene de vecino a la primer ciudad
                && (costoActual + ciudades[ciudad].vecinos[i].costo) < *costomaximo ) {

                caminoActual[ciudad] = &(ciudades[ciudad].vecinos[i]);
                copiar_camino(caminoActual, mejorCamino, cantidadCiudades);
                *costomaximo = costoActual + ciudades[ciudad].vecinos[i].costo;
            }
        }

    } else {
        for(int i = 0; i < ciudades[ciudad].grado; i++){
            ciudadSiguiente = ciudades[ciudad].vecinos[i].indice;

            if((costoActual + ciudades[ciudad].vecinos[i].costo) < *costomaximo
                && !caminoActual[ciudadSiguiente]) {

                caminoActual[ciudad] = &(ciudades[ciudad].vecinos[i]);
                dfs(ciudadSiguiente, ciudades, cantidadCiudades, costomaximo, caminoActual,
                    mejorCamino, costoActual+ciudades[ciudad].vecinos[i].costo, ciudadesVisitadas+1);
            }

        }
    }

    caminoActual[ciudad] = NULL;
}

/**
salida: Vertice*, int, Arista*[], char*
Recibe el arreglo con ciudades, la cantidad de ciudades, un array de punteros a arista
que reepresenta el ciclo, y el nombre del archivo de salida.
Imprime el camino en el archivo de salida.
*/
void salida(Vertice* ciudades, int cantidadCiudades, Arista* camino[], char salida[]) {
    int j = 0;
    FILE* archivoSalida;
    archivoSalida = fopen(salida, "w");
    Arista* aux;

    for (int i = 0; i < cantidadCiudades; ++i) {
        aux = camino[j];
        fprintf(archivoSalida, "%s,%s,%i \n", ciudades[j].etiqueta, ciudades[aux->indice].etiqueta, aux->costo);
        j = camino[j]->indice;
    }
    fclose(archivoSalida);
}

/**
encontrar_ciclo: Vertice*, int, char*
Recibe el arreglo con ciudades, la cantidad de ciudades, y el nombre del archivo
de salida. Declara e inicializa el camino actual y el costo maximo, y declara el
mejor camino. Hace el primer llamado a la funcion dfs con la primer ciudad, y los
parametros iniciales. Luego llama a la funcion salida pasandole de parametro el
mejor camino.
*/
void encontrar_ciclo(Vertice* ciudades, int cantidadCiudades, char archivoSalida[]){
    int costomaximo;
    Arista *caminoActual[cantidadCiudades], *mejorCamino[cantidadCiudades];

    costomaximo = INT_MAX;

    for (int i = 0; i < cantidadCiudades; ++i) {
        caminoActual[i] = NULL;
    }

    dfs(0, ciudades, cantidadCiudades, &costomaximo, caminoActual, mejorCamino, 0, 1);
    salida(ciudades, cantidadCiudades, mejorCamino, archivoSalida);
}

/**
liberar_memoria: Vertice*, int
Recibe el array de ciudades y la cantidad de ciudades.
Libera toda la memoria asignada.
*/
void liberar_memoria(Vertice* ciudades, int cantidadCiudades) {
    for (int i = 0; i < cantidadCiudades; ++i) {
        free(ciudades[i].vecinos);
    }
    free(ciudades);
}

int main(int argc, char const *argv[])
{
    int cantidadCiudades;
    Vertice* ciudades;
    char nombreArchivoEntrada[50], nombreArchivoSalida[50];

    strcpy(nombreArchivoEntrada, argv[1]);
    strcpy(nombreArchivoSalida, argv[2]);

    cantidadCiudades = entrada(nombreArchivoEntrada, &ciudades);

    //mostrar_lista(ciudades, cantidadCiudades); //Descomentar declaracion
                                                 //en caso de llamarla. 

    encontrar_ciclo(ciudades, cantidadCiudades, nombreArchivoSalida);
    liberar_memoria(ciudades, cantidadCiudades);

    return 0;
}
