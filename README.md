# Ignacio Rassol - TP Final - Estructura de Datos y Algoritmos I
## Archivos
El trabajo practico consta de un solo archivo, "main.c". Dado que el programa es relativamente corto en
lineas de codigo, y por como es el programa en si, no me parecio necesario dividirlo en varios archivos
para mejorar su legibilidad.


#
## Especificaciones

### __Estructuras utilizadas__:
Se representa el grafo como una lista de estructuras vertice, donde cada vertice contiene una lista de aristas, el nombre de la ciudad, y la cantidad de vecinos (el grado). Las aristas son estructuras que 
contienen el indice de la ciudad a la que van, y el costo. Esta es la unica estructura utilizada,
su declaracion se encuentra al principio del archivo "main.c".

### __Observaciones__:

-Comando de compilacion: gcc -o ejecutable -g -std=c99 -Wall -Wpedantic main.c

-Comando de ejecucion: ./ejecutable nombredelarchivodeentrada nombredelarchivodesalida

Hay mas observaciones respecto al criterio tomado sobre el programa en comentarios en las funciones
correspondientes en el codigo. Se priorizo que el codigo fuera simple, eficiente, y legible.

### __Flujo del programa__:
Se debe compilar "main.c".
Se debe ejecutar el programa especificando el nombre del archivo de entrada y el nombre del archivo
de salida por consola. Se asume que el archivo de entrada existe, y respeta el formato especificado.
Se comienza contando la cantidad de ciudades, luego se declara la memoria correspondiente y se lee 
el nombre de las ciudades. Luego se leen todas las aristas y se guardan en las ciudades correspondientes, y finalmente se ordenan las aristas de menor a mayor segun costo, y se cierra el archivo.
Una vez hecho esto, se comienza a buscar el ciclo. Para hacer esto, se recorren recursivamente todos los
caminos posibles, con las salvedades de que no se puede visitar una ciudad mas de una vez, que si el camino
actual es mas costoso que un ciclo encontrado previamente no se continua, y que una vez visitadas todas las
ciudades una unica vez, se tiene que volver al origen. Se va guardando el camino recorrido en un
arreglo que contiene punteros a las aristas que se usaron para desplazarse. Este camino se inicializa
previamente con todos NULL, y tambien se usa para verificar si ya se visito, o no, una ciudad. Cuando se
encuentra un ciclo, el camino reccorrido se copia en otro arreglo que no se vuelve a modificar, 
excepto que se encuentre un ciclo mejor.
Una vez explorados todos los caminos posibles, se imprimen los resultados en el archivo de salida,
y se libera memoria.

#
## Entrada
El archivo de entrada tiene que seguir lo pedido en el enunciado. Es decir, debe tener una lista
de nombres de ciudades, las cuales no deben contener espacios en blanco, separadas por una coma
y un espacio en blanco, y una lista de las aristas con sus costos. Ejemplo:

Ciudades  
a, b, c, d, e, f, g, h, i, j, k  
Costos  
a,b,3  
b,c,4  
a,e,7  
a,h,6  
  .  
  .  
  .  
k,g,2  

La ultima fila del archivo debe tener un salto de linea.

#
## Salida
La salida es un archivo que contiene las aristas con los costos que forman el ciclo de menor costo.
Ejemplo:
a,d,109   
d,f,17   
f,q,1258   
q,g,169   
  .  
  .  
  .  
k,a,661   

